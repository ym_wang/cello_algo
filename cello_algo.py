import os 
import itertools  
import numpy as np
import matplotlib.pyplot as plt
import configparser

""" 
Version Information:
    Version number: cello_algo_v0.7
    update content:
        1. Add two external algorithms read_ image,save_ image
        2. read_ Image reads the image buffer from the image file to call the image processing class @ 83
        3. save_ Image saves the processed image to the specified file @ 99
    Update time: July 8, 2021 13:27:03
"""
def read_ini():
    file = "./config.ini"
    conf = configparser.ConfigParser()
    if os.path.exists(file):
        conf.read(file)
        ncol = conf.get("ImageSize", "ncol")
        nrow = conf.get("ImageSize", "nrow")
        lens_x = conf.get("ImageSize", "lens_x")
        lens_y = conf.get("ImageSize", "lens_y")
        FSD = conf.get("ImageSize", "FSD")
        return (ncol, nrow, lens_x, lens_y, FSD)
    else:
        conf.add_section("ImageSize")
        conf.set("ImageSize", "ncol", "336")
        conf.set("ImageSize", "nrow", "336")
        conf.set("ImageSize", "lens_x", "14")
        conf.set("ImageSize", "lens_y", "14")
        conf.set("ImageSize", "FSD", "4096")
        with open(file, "w+") as f:
            conf.write(f)
        conf.read(file)
        ncol = conf.get("ImageSize", "ncol")
        nrow = conf.get("ImageSize", "nrow")
        lens_x = conf.get("ImageSize", "lens_x")
        lens_y = conf.get("ImageSize", "lens_y")
        FSD = conf.get("ImageSize", "FSD")
        return (ncol, nrow, lens_x, lens_y, FSD)

[sncol, snrow, slens_x, slens_y, sFSD] = read_ini()
[ncol, nrow, lens_x, lens_y, FSD] = \
    [int(sncol), int(snrow), int(slens_x), int(slens_y), int(sFSD)]

def extract_folder(data_dir,ncol_u,nrow_u,met):
    """
    load path and return image buffer and number of image
    Input:
        datta_dir : image path;
        ncol_u: image column;
        nrow_u: image row;
        met: image pixel bit
    Return:
        index: Number of sheets list;
        data_img: picture data list;
    """
    folder = [f for f in os.listdir(data_dir) if 'cap.raw' in f]
    data_img = np.zeros((ncol,nrow,len(folder)))
    index = np.zeros((len(folder),))
    i = 0
    
    for x in folder:
        folder.sort(key=lambda x: int(x.split('_')[0].split('.')[0]))
    
    for f in folder:
        f_sp = f.split('_')
        if met==16:
            index[i]=int(f_sp[0].split('.')[0],met)
        else :
            index[i]=float(f_sp[0].split('.')[0])

        raw_data = np.fromfile(os.path.join(data_dir, f), dtype=np.uint16)
        data_img[:,:,i] = np.reshape(raw_data, (ncol_u, nrow_u))
        i=i+1
    
    return index,data_img


def read_image(image_name):
    """
    read_image:
        read image buffer from file
    Input:
        image_name: The name of the file to read
    Return:
        img: The buffer read from file 
    """
    if os.path.exists(image_name):
        img = np.fromfile(image_name, dtype = np.uint16)
        img.reshape((336, 336))
    else:
        print(image_name, "is not exist!")

    return img

def save_image(image, image_name):
    """
    save_img:
        save image to file
    Input:
        image: image buffer to save
        image_name: The name of the file to save
    Return:
        None
    """
    image.tofile(image_name)

def get_lens_shift_per_lens(subimage:np.array=None):
    """
    Input: 
        lens : 14pixel * 14pixel
    Return: 
        lens_shift_x, lens_shift_y
    """
    # check incoming subimage size
    n_subimage_row = subimage.shape[0]
    n_subimage_col = subimage.shape[1]

    gray_index = 0
    gray = np.zeros(4096, dtype=np.uint16)

    pixel_num = n_subimage_row * n_subimage_col
    pixel_num_t = 0
    threshold = 0

    for row in range(n_subimage_row):
        for col in range(n_subimage_col):
            gray_index = int(subimage[row][col])
            gray[gray_index] += 1

    for i in range(4096)[::-1]:
        pixel_num_t += gray[i]
        if pixel_num_t > (pixel_num * 0.2):
            threshold = i
            break

    row_sum = col_sum = 0
    sum_count = 0
    for row in range(n_subimage_row):
        for col in range(n_subimage_col):
            if subimage[row][col] > threshold:
                row_sum = row_sum + row
                col_sum = col_sum + col
                sum_count = sum_count + 1

    if sum_count != 0:
        shift_in_row = row_sum / sum_count - (n_subimage_row - 1) / 2
        shift_in_col = col_sum / sum_count - (n_subimage_col - 1) / 2
    else:
        print("Can not calculate lens shift, Please try another picture!\n")
        shift_in_row = 0
        shift_in_col = 0

    return float(shift_in_col),float(shift_in_row)

class ProcessImg():
    """
    Class one：
        Process a single picture
    Input：
        image : A single picture
    method:
        lens mean:
        white_dot:
        lens_shift:
        plot_col_mean:
        plot_col_std:
        polt_lens_endline_mean:
    """
    def __init__(self, img):
        self.data_img = img

    def lens_mean(self, cstart, cstop, rstart, rstop):
        """ 
        lens_mean:
            Calculate the mean value of lens
        Innput:
            cstart : lens column start 0 ~ cstop
            cstop : lens coulumn stop cstart ~ 23
            rstart : lens row start 0 ~ rstop
            rstop : lens row stop rstart ~ 23
        Return:
            lens mean
        """
        image_raw = np.reshape(self.data_img, (lens_x, int(ncol / lens_x), lens_y, int(nrow / lens_y)))
        lens_mean = np.zeros(lens_x * lens_y, dtype = np.uint16)
        for i in range(lens_x) :
            for j in range(lens_y):
                lens_mean[i * lens_y + j] = np.mean(image_raw[i , cstart : cstop, j, rstart : rstop])
        
        return(lens_mean)

    def white_dot(self):
        """ 
        white_dot:
            Calculate the number of white spots
        Input: 
            None
        Return：
            num : number of white spots
        """
        pixel_data = list(itertools.chain(*self.data_img))
        pixeldata = np.sort(pixel_data)
        A = ncol * nrow * (1-0.03)
        B = int(A)
        Threshold = 2 * pixeldata[B]
        img = np.reshape(self.data_img, (ncol, nrow))
        mean = np.mean(self.data_img)
        std = np.std(self.data_img)
        num = np.zeros(7,)

        for m in range(ncol):
            for n in range(nrow):
                if img[m][n] - 10 * mean > 0:
                    num[0] += 1
                if  img[m][n] - Threshold > 0:
                    num[1] += 1
                if img[m][n] - 10 * mean > 0 and img[m][n] - Threshold > 0:
                    num[2] += 1
                if img[m][n] - (mean + 3 * std) > 0:
                    num[3] += 1
                if img[m][n] - (mean + 18 * std) > 0:
                    num[4] += 1
                if img[m][n] - (mean + 36 * std) > 0:
                    num[5] += 1
                
        return(num)
        
    def lens_shift(self):
        """
        lens_shift:
            Calculate the lens_shift of a picture
        Input:
            None
        Return:
            lens_shift_x,lens_shift_y
        """
        image_raw = self.data_img
        image_raw = image_raw.reshape((lens_x, int(ncol / lens_x), lens_y, int(nrow / lens_y)))
        num = 0
        lens_shift_x = 0
        lens_shift_y = 0
        for i in range(3, 11, 1) :
            for j in range(3, 11, 1):
                a, b = get_lens_shift_per_lens(image_raw[i,2:22,j,2:22])
                lens_shift_x += a 
                lens_shift_y += b
                num += 1

        return lens_shift_x / num, lens_shift_y / num

    def plot_col_mean(self):
        """
        plot_col_std:
            plot column mean of one picture;
        Input:
            None
        Return:
            None
        Plot:
            column mean VS column value
        """
        num = np.zeros(ncol, dtype=np.int16)
        col_mean = np.zeros(ncol, dtype=np.float16)

        for i in range(ncol):
            num[i] = i
            col_mean[i] = np.mean(self.data_img[i:i + 1, :])

        plt.figure(0)
        plt.plot(num,col_mean,'*')
        plt.xlabel("column value")                        
        plt.ylabel("mean value")
        plt.title('col mean VS column value')
        plt.show()

    def plot_col_std(self):
        """
        plot_col_std:
            plot column standard deviation of one picture;
        Input:
            None
        Return:
            None
        Plot:
            column std VS column value
        """
        num = np.zeros(ncol, dtype=np.int16)
        col_std = np.zeros(ncol, dtype=np.float16)

        for i in range(ncol):
            num[i] = i
            col_std[i] = np.std(self.data_img[i:i + 1, :])

        plt.figure(0)
        plt.plot(num,col_std,'*')
        plt.xlabel("column value")                        
        plt.ylabel("std value")
        plt.title('col std VS column value')
        plt.show()
        
    def plot_lens_endline_mean(self):
        """
        plot_col_std:
            plot lens endline mean of one picture;
        Input:
            None
        Return:
            None
        Plot:
            mean value VS lens endline number
        """
        num = np.zeros(lens_y, dtype=np.int16)
        col_lens = np.zeros(lens_y, dtype=np.float16)

        for i in range(lens_y):
            num[i] = i
            a = 24 * i + 23
            print(a)
            col_lens[i] = np.mean(self.data_img[a : a + 1, : ])

        plt.figure(0)
        plt.plot(num,col_lens,'*')
        plt.xlabel("lens endline number")                        
        plt.ylabel("mean value")
        plt.title('mean value VS lens endline number')
        plt.show()
 
class ProcessFolder():
    """
    Class two：
        Process a set of pictures
    Input：
        The path of picture
    method：
        plot_mean:
        polt_sensitivity:
        plot_std:
        ncr_rate:
        temp_noise:
        vfpn:
        hfpn:
        batch_lens_shift:
    """
    def __init__(self, path):
        self.path = path 
        [index,data_img] = extract_folder(path,ncol,nrow,met = 12)
        img = np.zeros(len(index),dtype = ProcessImg)
        for i in range(int(len(index))):
            img[i] = ProcessImg(data_img[:,:,i])
        self.index = index
        self.img = img
        self.data_img = data_img
        self.xlabelname = self.path.split("\\")[-1] + ' value'
        
    def polt_mean(self, cstart, cstop, rstart, rstop):
        ''' 
        polt_mean:
            Polt mean value of a group of pictures;
        Input:
            cstart : lens column start 0 ~ cstop
            cstop : lens coulumn stop cstart ~ 23
            rstart : lens row start 0 ~ rstop
            rstop : lens row stop rstart ~ 23
        Return:
            None;
        Polt:
            Mean vs Inttime
        '''
        data_mean = np.zeros((len(self.index),))
        csv_mean = self.path + "\\" + "mean.csv"
        with open(csv_mean, "w") as file_csv:
            for i in range(len(self.index)):
                data_mean[i] = np.mean(self.img[i].lens_mean(cstart, cstop, rstart, rstop))
                data = str(i) + "," + str(data_mean[i]) + "," + "\r"
                file_csv.write(data)

        plt.figure(0)
        plt.plot(self.index,data_mean,'*')
        plt.xlabel(self.xlabelname)                        
        plt.ylabel("mean" + " value")
        plt.title('img mean vs ' + self.xlabelname)
        plt.show()

    def polt_sensitivity(self, start, stop, sill):
        ''' 
        polt_sensitivity:  
            polt mean + add fitting curve 
        Input:
            start: The intergration time of start
            stop: The intergration time of stop
            sill: K's card control threshold
        Return:
            K, B
        Polt:
            Mean vs inttime
        '''
        data_mean = np.zeros((len(self.index),))
        for i in range(len(self.index)):
            data_mean[i] = np.mean(self.img[i].lens_mean(8,16,8,16))

        plt.figure(0)
        plt.plot(self.index,data_mean,'*')
        plt.xlabel(self.xlabelname)                        
        plt.ylabel("mean" + " value")
        plt.title('img mean vs ' + self.xlabelname)
        plt.show()

        kmean = (data_mean[stop] - data_mean[start]) / (stop - start)
        knum = 0
        bnum = 0
        num = 0
        for i in range(start, stop, 1):
            k = (data_mean[i + 1] - data_mean[i])
            b = data_mean[i] - k * i

            if (k < (kmean + sill) and k > (kmean - sill)):
                knum += k
                bnum += b 
                num += 1

        return knum / num, bnum / num


    def polt_std(self):
        """ 
        polt_std:
            Polt standard deviation of a set of pictures;
        Input:
            None;
        Return：
            None;
        Polt:
            STD vs Inittime
        """
        data_std = np.zeros((len(self.index),))
        for i in range(len(self.index)):
            data_std[i] = np.std(self.data_img[:,:,i])

        plt.figure(1)
        plt.plot(self.index,data_std,'*')
        plt.xlabel(self.xlabelname)                        
        plt.ylabel("std" + " value")
        plt.title('img std vs ' + self.xlabelname)
        plt.show()

    def ncr_rate(self):
        """ 
        ncr_rate:
            statistics NCR rate 
        Input：
            None
        Return:
            ncr13 count, cnr26_count, ncr27 count;
        """
        ncr13_count = 0
        ncr26_count = 0
        ncr27_count = 0
        
        diff01 = np.zeros((len(self.index),))
        diff12 = np.zeros((len(self.index),))
        
        for i in range(len(self.index)):
            raw2 = np.reshape(self.data_img[:,:,i], (14, int(ncol/14), 14, int(nrow/14)))

            diff01[i]=raw2[0,0:6,:,:].mean()-raw2[1,0:6,:,:].mean()
            diff12[i]=raw2[1,0:6,:,:].mean()-raw2[2,0:6,:,:].mean()
            if(np.abs(diff01[i])>np.abs(diff12[i])*3):
                ncr27_count=ncr27_count+1

            diff01[int(i)]=raw2[0:3,:,:,:].mean()
            diff12[int(i)]=raw2[10:13,:,:,:].mean()
            if(np.abs(diff12[int(i)])>np.abs(diff01[int(i)])):
                ncr26_count=ncr26_count+1
                
            num = 0
            for a in range(14):
                for b in range(20, 23, 1):
                    diff01[i]=raw2[a, b : b + 1, : , : ].mean()
                    diff12[i]=raw2[a, b + 1 : b + 2, : , : ].mean()
                    if(np.abs(diff12[i]) > np.abs(diff01[i])):
                        num += 1
                        break
            if num != 0 :
                ncr13_count=ncr13_count+1
                
                
        print("**********NCR13*********")
        print("number of NCR13 --> " + str(ncr13_count))
        print(ncr13_count / len(self.index) * 100)
        print("************************\n")
        print("number of NCR26 --> " + str(ncr26_count))
        print(ncr26_count / len(self.index) * 100)
        print("************************\n")
        print("**********NCR27*********")
        print("number of NCR27 --> " + str(ncr27_count))
        print(ncr27_count / len(self.index) * 100)
        print("************************\n")
    
        return ncr13_count, ncr26_count, ncr27_count

    def temp_noise(self):
        """ 
        temp_noise:
            temporal noise 
        Input:
            None
        Return: 
            None
        """
        img_std=self.data_img.std(2)
        sum_std = 0.0
        for i in range(0,nrow):
            for j in range(0,ncol):
                sum_std += img_std[i][j] ** 2
        
        std = np.sqrt(sum_std / (nrow * ncol))
        print("these",len(self.index),"images Standard deviation is",std)
        
    def vfpn(self):
        """ 
        VFPN:
            Calculate the VFPN of a set of pictures;
        Input:
            None
        Return:
            None
        """
        tmp = self.data_img.mean(2).mean(1)
        
        tmpl=0
        for i in range(ncol-1):
            tmpl = tmpl + (tmp[i + 1] - tmp[i]) ** 2
        
        VFPN_level = np.sqrt(tmpl / (ncol - 1)) 
        
        Vwin = 5;  ##with window of Vwin*2+1=11
        tmpavg = np.zeros(ncol - Vwin * 2 - 1)
        for i in range(ncol - Vwin * 2 - 1):
            tmpavg[i] = tmp[i : i + Vwin * 2].mean(0)
        tmpdelta = np.absolute(tmp[Vwin : ncol - Vwin - 1] - tmpavg)
        VFPN_max = tmpdelta.max(0) 
        
        VFPN_max_loc = np.where(tmpdelta == tmpdelta.max(0))
        print("\n","VFPN_level is ",VFPN_level,"\n", "VFPN_max is ", VFPN_max,"\n", "located at column ",VFPN_max_loc, "\n")

    def hfpn(self):
        """ 
        HFPN:
            Calculate the HFPN of a set of pictures;
        Input:
            None
        Return:
            None
        """
        
        tmp = self.data_img.mean(2).mean(0)
        tmpl = 0
        for i in range(nrow - 1):
            tmpl=tmpl+(tmp[i + 1] - tmp[i]) ** 2
        
        HFPN_level = np.sqrt(tmpl / (nrow - 1)) 
        Hwin = 5  ##with window of Vwin*2+1=11
        
        tmpavg = np.zeros(nrow - Hwin * 2 - 1)
        for i in range(nrow - Hwin * 2 - 1):
            tmpavg[i] = tmp[i : i + Hwin * 2].mean(0)
        
        tmpdelta = np.absolute(tmp[Hwin : nrow - Hwin - 1] - tmpavg)
        
        HFPN_max = tmpdelta.max(0) 
        HFPN_max_loc = np.where(tmpdelta == tmpdelta.max(0))
        print("\n","HFPN_level is ",HFPN_level,"\n", "HFPN_max is ", HFPN_max,"\n", "located at row ",HFPN_max_loc, "\n")

    def batch_lens_shift(self):
        """
        batch_lens_shift:
            Calculate a set of pictures lens shift
        Input:
            None;
        Return:
            lens_shift
        """
        lens_shift = np.zeros((len(self.index), 2), dtype=np.float32)
        for i in range(len(self.index)):
            lens_shift[i] = self.img[i].lens_shift()

        return lens_shift
