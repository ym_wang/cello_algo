## cello algo v0.7

### 文件介绍：

---

cello_algo.py：文件中是包含算法所使用的基础函数和接口调用；

cello_demo.py：这里是一个小的测试demo，测试人员可以根据自己的需求修改测试demo来实现具体的内容；

config.ini：用来配置要处理的图片的参数。

### 使用方法：

---

1. 确保两个文件在统一目录下；
2. 用Spyder打开cello_demo.py文件；
3. 添加自己的测试项；
4. 点击"F5"进行测试。

### 接口解释：

---

**类ProcessFolder:**

输入：文件路径：

​	plot_mean(cstart, cstop, rstart, rstop):把每张图片的mean值和积分时间对应起来plot显示；

​    polt_sensitivity: 把每张图片的mean值和积分时间对应起来plot显示，返回K,B值

​    plot_std:把每张图片的std值和积分时间对应起来plot显示；

​    ncr_rate:计算NCR-13, NCR-26, NCR-27出现的概率

​    temp_noise:计算一组图片的temporal_noise。

​    vfpn:计算一组图片的vfpn；

​    hfpn:计算一组图片的hfpn；

​    batch_lens_shift:计算一组图片的lens shift

**类ProcessImg：**

输入：一张图片：

1. lens_mean(cstart, cstop, rstart, rstop): 计算第number张图片的lens mean值；
2. white_dot():计算第number张图片的白点的个数；
3. lens_shift():计算sensor的lens shift
4. plot_col_mean:plot column mean；
5. plot_col_std:plot column std；
6. plot_lens_endline_mean；

**方法read_image:**

功能：从文件中读取image buffer ，用来调用ProcessImg时使用；

输入：包含绝对路径的文件名：E:\share\20210707\test\capture\100_100_0_1614837276_cap.raw；

返回：读取出来的图片buffer；

**方法save_image:**

功能：把处理完的图片buffer 保存成指定的文件；

输入：图片buffer & 要保存的文件名；

返回：None

### 修改说明：

---

#### v0.5

1. 增加read_ini方法，load config.ini文件。
2. 修改polt_mean，可以选择polt的区域；
3. 在polt_mean里面增加save csv文件的功能。

#### v0.6

1. 优化NCR-13的算法；
2. 新增plot column mean；
3. 新增plot column std；
4. 新增plot lens endline mean；

#### v0.7

1. 增加两个对外的算法read_image,save_image
2. read_image从图片文件里面读取图片buffer用来调用图片处理类
3. save_image把处理完的图片保存到指定文件中



