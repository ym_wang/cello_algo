import os 
import sys 
import itertools  
import numpy as np
import matplotlib.pyplot as plt
from numpy.lib.function_base import insert

from cello_algo import ProcessImg as Img
from cello_algo import ProcessFolder as Folder

##sensor general test condition
FPCLK=25e6;
Tpclk=1/FPCLK;
FrameWidth=4736;
Trow=Tpclk*FrameWidth;
Tint1=128*Trow;
Tint2=336*Trow;


path1 = r'E:\cello\image\0x0080'
#path 1 is the result with Tint=0x080
batch1 = Folder(path1)
data_image1 = batch1.data_img
mean_img1 = data_image1.mean(2)

path2 = r'E:\cello\image\0x0150'
batch2 = Folder(path2)
#path 1 is the result with Tint=0x0150
data_image2 = batch2.data_img
mean_img2 = data_image2.mean(2)

DSNU_ms = np.std(mean_img2 - mean_img1) / (Tint2 - Tint1) / 1000
###dark signal non-uniformaty per ms

###For reference, we calculate std per pixel for each Tint; 


print('per pixel std for Tint1 is ',np.std(data_image1[:,:,0] - data_image1[:,:,1]))
print('per pixel std for Tint2 is ',np.std(data_image2[:,:,0] - data_image2[:,:,1]))
print('calculated std for Tint1-Tint2 is ',np.std(mean_img2 - mean_img1) )
print('calculated DSNU is ',DSNU_ms, 'per ms')




# batch.polt_mean()
# batch.ncr_number()

# img = Img(path)
# img.lens_mean(100)
# img.white_dot(10)
